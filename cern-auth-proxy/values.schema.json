{
    "$schema": "http://json-schema.org/draft-07/schema",
    "type": "object",
    "required": [
        "route",
        "upstream"
    ],
    "properties": {
        "upstream": {
            "type": "object",
            "title": "Upstream Application",
            "description": "Define the applications which should be protected by SSO.",
            "required": [
                "service"
            ],
            "properties": {
                "service": {
                    "type": "object",
                    "title": "Service definition",
                    "description": "Define the target service which should be protected by SSO.",
                    "required": [
                        "name",
                        "port"
                    ],
                    "properties": {
                        "name": {
                            "type": "string",
                            "title": "Service name",
                            "description": "The name of the service (e.g. 'node-js-ex')"
                        },
                        "port": {
                            "type": "integer",
                            "title": "Port",
                            "description": "The port of the service (e.g. '8080')"
                        }
                    },
                    "additionalProperties": true
                }
            },
            "additionalProperties": true
        },
        "route": {
            "type": "object",
            "title": "Routing Configuration",
            "description": "Advanced options for configuring the application routing.",
            "properties": {
                "hostname": {
                    "type": "string",
                    "title": "Public Application Hostname",
                    "description": "The hostname under which your application should be accessible (without http://)",
                    "examples": [
                        "node-js-ex.app.cern.ch"
                    ]
                },
                "create": {
                    "type": "boolean",
                    "title": "Create a route",
                    "description": "Set to false to skip creating a route for the SSO proxy. Useful when using the proxy is used for authentication subrequests in nginx.",
                    "examples": [
                        true
                    ]
                },
                "path": {
                    "type": "string",
                    "title": "Application Subpath",
                    "description": "The subpath of the application that gets protected by SSO proxy (e.g. '/admin' to only require authorization for that path)",
                    "examples": [
                        "/"
                    ]
                },
                "public": {
                    "type": "boolean",
                    "title": "Internet Visibility",
                    "description": "Whether your application should be accessible only from CERN network (false) or from the Internet (true)",
                    "examples": [
                        true
                    ]
                },
                "annotations": {
                    "type": "object",
                    "title": "Route Annotations",
                    "patternProperties": {
                        ".+": { "type": "string" }
                    }
                },
                "annotationsArray": {
                    "type": "array",
                    "title": "Route Annotations",
                    "description": "Additional annotations to be added to the route",
                    "additionalItems": true,
                    "items": {
                        "type": "string",
                        "title": "Route Annotation",
                        "description": ""
                    }
                }
            },
            "additionalProperties": true
        },
        "authOptions": {
            "type": "object",
            "title": "Authentication Options",
            "description": "Advanced options for the SSO integration.",
            "properties": {
                "allowedRole": {
                    "type": "string",
                    "title": "Allowed Role",
                    "description": "Specifies which Application Role is required to access the application; This is NOT an e-group but the Role Identifier of a Role on the CERN Application Portal (where each application defines it own set of roles); New applications are initialized with a `default-role` containing all CERN + EduGain users; The default-role can be modified to include only members of an e-group.",
                    "examples": [
                        "default-role"
                    ]
                },
                "proxyPrefix": {
                    "type": "string",
                    "title": "Oauth Proxy Prefix",
                    "description": "The subpath on which the Oauth proxy is listening; This path MUST NOT be used by the application itself, since requests under this path ARE NOT FORWARDED.",
                    "examples": [
                        "/oauth2"
                    ]
                },
                "preferEmail": {
                    "type": "boolean",
                    "title": "Prefer Email Address",
                    "description": "Use the email address as the username when passing information to the application ('true' or 'false').",
                    "examples": [
                        "true"
                    ]
                },
                "extraArgs": {
                    "type": "array",
                    "title": "Extra Arguments",
                    "description": "Additional Arguments for Oauth2 Proxy server.",
                    "additionalItems": true,
                    "items": {
                        "type": "string",
                        "title": "Oauth2 Proxy Argument",
                        "description": ""
                    }
                }
            },
            "additionalProperties": true
        }
    },
    "additionalProperties": true
}
