Shortly, your application will be available at:
  https://{{ .Values.route.hostname }}/{{ trimAll "/" .Values.route.path }}
