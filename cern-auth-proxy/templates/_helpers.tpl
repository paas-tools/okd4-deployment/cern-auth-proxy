{{/*
Expand the name of the chart.
*/}}
{{- define "cern-auth-proxy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "cern-auth-proxy.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "cern-auth-proxy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "cern-auth-proxy.labels" -}}
helm.sh/chart: {{ include "cern-auth-proxy.chart" . }}
{{ include "cern-auth-proxy.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ include "cern-auth-proxy.chart" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "cern-auth-proxy.selectorLabels" -}}
app.kubernetes.io/name: {{ include "cern-auth-proxy.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
We just use the default service account of the namespace.
*/}}
{{- define "cern-auth-proxy.serviceAccountName" -}}
{{- print "default" }}
{{- end }}

{{/*
Generate a proxy prefix with leading slash and without trailing slash
based on authOptions.proxyPrefix and route.path
*/}}
{{- define "cern-auth-proxy.proxyPrefix" -}}
{{- $pp := .Values.authOptions.proxyPrefix | default "/oauth2" }}
{{- $r := "" }}
{{- if and .Values.route.path (ne .Values.route.path "/") }}
{{- $r = print "/" (trimAll "/" .Values.route.path) }}
{{- end }}
{{- printf "%s/%s" $r (trimAll "/" $pp) -}}
{{- end }}

{{/*
Generate Upstream URL for Proxy: http://HOST:PORT/PATH
*/}}
{{- define "cern-auth-proxy.upstreamUrl" -}}
{{- printf "http://%s:%v/" .Values.upstream.service.name .Values.upstream.service.port -}}
{{- end }}

{{/*
URL to redirect to after authentication
*/}}
{{- define "cern-auth-proxy.redirectUri" -}}
{{- $pp := (include "cern-auth-proxy.proxyPrefix" .) }}
{{- printf "https://%s%s/callback" .Values.route.hostname $pp -}}
{{- end }}
