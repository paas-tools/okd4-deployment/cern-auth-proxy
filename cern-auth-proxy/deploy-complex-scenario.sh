#!/bin/sh

set -e

APP_NAME=example-app
APP_PORT=80
HOSTNAME=cern-auth-proxy-test.webtest.cern.ch

oc apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: $APP_NAME
  labels:
    app: $APP_NAME
    instance: test
spec:
  containers:
  - image: docker.io/containous/whoami
    imagePullPolicy: IfNotPresent
    name: $APP_NAME
    args: ["-port", "8080"]
    ports:
    - containerPort: 8080
      protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: $APP_NAME
  labels:
    app: $APP_NAME
    instance: test
spec:
  ports:
  - name: "$APP_PORT"
    port: $APP_PORT
    protocol: TCP
    targetPort: 8080
  selector:
    app: $APP_NAME
    instance: test
  type: ClusterIP
---
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  name: ${APP_NAME}-public
  labels:
    app: $APP_NAME
    instance: test
spec:
  host: cern-auth-proxy-test.webtest.cern.ch
  path: /public
  port:
    targetPort: $APP_PORT
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
  to:
    kind: Service
    name: $APP_NAME
EOF

cat - <<EOF > /tmp/cern-auth-proxy-test-1.yaml
upstream:
  service:
    name: $APP_NAME
    port: $APP_PORT
route:
  hostname: $HOSTNAME
  path: /
  # allowedRole defaults to 'default-role'
EOF

cat - <<EOF > /tmp/cern-auth-proxy-test-2.yaml
upstream:
  service:
    name: $APP_NAME
    port: $APP_PORT
route:
  hostname: $HOSTNAME
  path: /admin
authOptions:
  allowedRole: admin-role
  preferEmail: false
EOF

helm upgrade --install -f /tmp/cern-auth-proxy-test-1.yaml proxy-test-1 ./

helm upgrade --install -f /tmp/cern-auth-proxy-test-2.yaml proxy-test-2 ./

cat - <<EOF
Everything deployed.
Use the following commands to clean up:

    oc delete pod,service,route -l app=$APP_NAME,instance=test
    helm uninstall proxy-test-1
    helm uninstall proxy-test-2

EOF
