# CERN Authentication Proxy

This project contains scripts / manifests for deploying an authentication proxy for the [new CERN SSO](https://auth.docs.cern.ch/) on [CERN PaaS](https://paas.docs.cern.ch/).
For end-user instructions (deployment etc.), please refer to ["Deploy SSO proxy" in the PaaS documentation](https://paas.docs.cern.ch/4._CERN_Authentication/2-deploy-sso-proxy/).

The chart is based on the open-source [Oauth2 Proxy project](https://github.com/openshift/oauth-proxy/).

A similar open-source Helm chart is available [here](https://github.com/oauth2-proxy/manifests/tree/main/helm/oauth2-proxy/templates).
It can be used as a reference for implementation details.

## Updating the ImageStream

This Helm chart does not directly use the [upstream oauth2-proxy container images](https://quay.io/oauth2-proxy/oauth2-proxy), but instead relies on an [OpenShift Image Stream](https://docs.openshift.com/container-platform/4.7/openshift_images/image-streams-manage.html#images-using-imagestream-tags_image-streams-managing) created in our clusters: [shared-image-streams Helm chart](https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/chart/charts/shared-image-streams).
This approach allows us to update the image without requiring any user action.

To test newer container images (without rolling out a new image to the imagestream), we can simply overwrite the internal image with the upstream image in [deployment.yaml](./cern-auth-proxy/templates/deployment.yaml) (*Note: this change should NOT be committed into the repo, it is only for testing purposes.*):

```diff
- image: image-registry.openshift-image-registry.svc:5000/openshift/oauth2-proxy:latest
+ image: quay.io/oauth2-proxy/oauth2-proxy:v7.2.0-amd64
```

Afterwards, test the new image by using the [manual integration testing instructions](#manual-integration-tests) at the bottom of this README.

## Helm Chart in OpenShift

The OpenShift Web Console can directly deploy Helm charts (see [Using Helm with Red Hat OpenShift](https://cloud.redhat.com/learn/topics/helm) and [Advanced Helm support in OpenShift 4.5 Web Console](https://developers.redhat.com/blog/2020/07/20/advanced-helm-support-in-the-openshift-4-5-web-console)).

It uses the values.schema.json to provide a nice UI for the user to fill out required and optional Helm values ([example values.schema.json](https://github.com/redhat-developer/redhat-helm-charts/blob/master/alpha/nodejs-ex-k/values.schema.json)).

Useful introduction and tools: [Better Helm Chart installation experience in OpenShift Console using values.schema.json](https://gist.github.com/rohitkrai03/b5ad757557ce68c7eb0e7e23eab94b25)

To make the Helm charts visible in the OpenShift Console, a Kubernetes object of Kind `HelmChartRepository` needs to be created.
Openshift will then provide all charts in the configured repository to the user.
For details refer to [Configuring Custom Helm Chart repositories](https://docs.openshift.com/container-platform/4.7/cli_reference/helm_cli/configuring-custom-helm-chart-repositories.html).

## Developing and Releasing

When developing this Helm chart we need to keep in mind that the structure of the `values.yaml` file should be a stable interface for the end-user, i.e. no breaking changes.
There are many ways to accommodate large chart restructurings without the need to change the values structure, e.g. by using helper templates.

To list all currently deployed instances of this Helm chart (along with their versions), the following command can be used:

```sh
oc get deploy -A -o json | jq -r '.items[] | select(.metadata.labels."helm.sh/chart" | test("cern-auth-proxy")?).metadata | .namespace + " " + .labels."helm.sh/chart"'
```

We need to increase the Chart version number (`version` in `Chart.yaml`) according to semantic versioning: `MAJOR.MINOR.BUGFIX`

* `MAJOR` releases may contain breaking changes (e.g. the values structure);
* `MINOR` releases may add features as long as they are backwards-compatible;
* `BUGFIX` releases only include minor corrections and are also backwards-compatible.

See [Helm Chart versioning](https://helm.sh/docs/topics/charts/#charts-and-versioning).

**In short**, with every modification that should be published the version number of the chart **must be** incremented.
Otherwise tools in the build- and deployment chain will fail to detect new versions etc.

Once the new version is merged to `master` the [CI pipeline](./.gitlab-ci.yml) packages the Helm chart and uploads it to Gitlab's package registry for this project.
The Helm repository is available at `https://gitlab.cern.ch/api/v4/projects/124069/packages/helm/<BRANCH_NAME>`

Note: `124069` is the *Project ID* of this Gitlab project.

## Linting

Run the [Helm chart linter](https://github.com/helm/chart-testing) with:

```
podman run --rm -v ${PWD}:/apps -w /apps quay.io/helmpack/chart-testing:v3.4.0 ct lint --all --chart-dirs .
```

## Unit tests

This chart is tested with [helm-unittests](https://github.com/quintush/helm-unittest).
Unit tests are placed in the `tests/` folder.
They do not require a live Kubernetes cluster to run.
Refer to the [testing document specification](https://github.com/quintush/helm-unittest/blob/master/DOCUMENT.md) or the [examples](https://github.com/quintush/helm-unittest/tree/master/test/data/v3/basic/tests).

You can run the tests locally with:

```
podman run --rm -v "${PWD}:/apps" docker.io/quintush/helm-unittest:3.6.3-0.2.7 --helm3 ./cern-auth-proxy/
```

## UI testing on your own cluster

To test the chart on your own (PaaS) OKD4 dev cluster, you can of course simply `helm install` the chart directly into your project:

```
helm install -f my-values.yaml my-sso ./cern-auth-proxy/
```

Alternatively, if you want to test what the chart looks like in the OpenShift Console, follow these steps:

1) Push your changes to a new branch on this project

2) Add the chart repository to your cluster
```
cat <<EOF | oc apply -f -
apiVersion: helm.openshift.io/v1beta1
kind: HelmChartRepository
metadata:
  name: jacks-helm-repo
spec:
  name: "Jack's Helm Repository"
  connectionConfig:
    url: https://gitlab.cern.ch/api/v4/projects/124069/packages/helm/<BRANCH_NAME>
EOF
```

4) Now you can install the chart through the OpenShift Web Console ("Add" -> "Helm Chart" -> "CERN Auth Proxy") - *make sure to select that version that comes from your newly added **repository** .*

## Manual integration tests

This guide details how to run a manual integration test on your dev cluster.
It deploys the [whoami example application](https://github.com/traefik/whoami) which returns HTTP headers as the response, which we can use to verify the correct authentication and proxy settings.

The `deploy-complex-scenario.sh` will configure an application with the following endpoints:

* `/` -> only available for authenticated users with `default-role`
* `/admin` -> only available for authenticated users with `admin-role`
* `/public` -> available for un-authenticated users

After creating your Openshift project and running `sh deploy-complex-scenario.sh`, head to <https://application-portal-qa.web.cern.ch/> to configure your application registration and go to the `Roles` tab.
The application already has the `default-role` role, which you can leave as is.
Add a new role with the following properties:

* Role Identifier: `admin-role`
* Role Name: `Admin Role`
* *do not check any of the optional settings*

=> `Submit`

Then, click on the green button to "Assign groups to role" and add the `openshift-cluster-admins` groups.

Now we can test the access restrictions for the three user groups:

### Unauthenticated users

* Open a private window in your browser (ensure they are no left-over cookies!)
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/> - you should be redirect to the login form.
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/admin> - you should be redirect to the login form.
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/public> - you should have access to this site. There should be NO `X-Forwarded-User` request header in the response shown on the webpage.

### Authenticated users

* Go to <https://cern-auth-proxy-test.webtest.cern.ch/> - you should be redirect to the login form. Log in with your regular CERN account. You should be redirect to the main page. There should be `X-Forwarded-User: your.email@cern.ch` and `X-Forwarded-Groups: default-role` headers as well as a rather large `Cookie: _oauth2_proxy_0=LOOOOONG_STRING
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/admin> - you should NOT have access to this site.
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/public> - you should have access to this site. There should be the same cookie as before, but NO `X-Forwarded-User` and `X-Forwarded-Groups` lines (because this request should not pass the through the oauth proxy).
* Close the private browser window / clear the cookies.

### Authenticated admins

* Go to <https://cern-auth-proxy-test.webtest.cern.ch/> - you should be redirect to the login form. Log in with your admin service account. You should be redirect to the main page. There should be the `X-Forwarded-User: admXXX` (**without `@cern.ch`**) and `X-Forwarded-Groups: default-role,admin-role` headers as well as a rather large `Cookie: _oauth2_proxy_0=...`
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/admin> - you should have access to this site.  There should be a `X-Forwarded-User: admXXX` header (**without `@cern.ch`**) as well as a rather large `Cookie: _oauth2_proxy_0=...`
* Go to <https://cern-auth-proxy-test.webtest.cern.ch/public> - you should have access to this site. There should be the same cookie as before, but NO `X-Forwarded-User` line (because this request should not pass the through the oauth proxy).
* Close the private browser window / clear the cookies.

*Integration tests: PASSED*
